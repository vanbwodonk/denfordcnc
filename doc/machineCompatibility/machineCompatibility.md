# Machine Compatibility

I designed the GRBL board for my machine, branded Denford CNC which uses the simpler Smartstep/3 controller and does not have a spindle speed output. The spindle speed is controlled using a potentiometer knob on the front panel. I have noticed that this model only has one knob on the front panel and generally the machines fitted with the Denstep and NextMove ST controllers have an extra knob for feed.

|Model|Notes|Compatibility|Testing|
|---|---|---|---|
|Denford Micromill or Scantek 2000 with Smartstep/3 controller |Has only spindle speed knob on the front panel|Plug and play compatible|Heavily tested on my machine and 5 others|
| Denford Micromill or Scantek 2000 with Denstep or NextMove ST Controller| Has two knobs on the front panel (Spindle speed and feed)|Spindle speed pot needs to be wired directly to speed controller. Feed speed is controlled though software.|Tested by 10+ people|
|Denford / Scantek 2000 MicroTURN|Can be optionally fitted with a spindle encoder needed for threading|GRBL is not compatible with spindle encoders and therefore can only do shaping not threading|Tested by 2 people|




<img src="doc/machineCompatibility/ControllerTypes.png" width="800">


Source: https://www.denfordata.com/projectimages/machine_upgrade_identification_guide.pdf


## Rewiring the Spindle speed pot and adding an enable switch


Please note that I do not own the machines that need to be rewired so I am doing the best I can to write documentation with the pictures I have found. Any help would be greatly appreciated.

Do not install the GRBL Board until you have made these changes.

Remove the feed potentiometer and wires (not needed anymore)

Disconnect the spindle speed wires from the Orange connectors and wire it according to this picture

<img src="doc/machineCompatibility/potentiometerPinout.png" width="200">

<img src="doc/machineCompatibility/spindleContolle.jpg" width="500">


In my case the two blue wires are pin 1 and 3 on the pot. The white wire is pin 2 (wiper).

If the pot is working but in the wrong direction switch P1 and P3.



Disconnect the Spindle enable wire from the main board. Add a switch to the front panel (you could use the feed rate pot hole) Connect the switch to ground. This will make the front panel switch control the spindle.

[Wiring Diagram](https://gitlab.com/damped/denfordcnc/-/blob/master/doc/wiringDiagrams/NextmoveST_rewiring.pdf)

Make sure to do this before you plug in the board I created so that you don't damage the spindle en features that are meant to be used with a relay.
